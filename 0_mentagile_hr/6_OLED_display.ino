#if OLED_disp_used
	// center and print a small one line title
  // "title" = string to print
  // "font"  = 0-3 as font size
  // "clsc"  = 1 to clean the screen, or 0 to add it to the current screen
	void printTitle(String title, uint8_t font, boolean clsc=1) {
		if (clsc) oled.clear(PAGE);
		oled.setFontType(font);
		// Set the cursor in the middle of the screen ish
		oled.setCursor(5,
						OLEDmiddleY - (oled.getFontHeight()));
		// Print the title:
		oled.print(title);
		oled.display();
	}

	// add sparkle to the screen
	void randomPixel() {
		for (uint8_t i=0; i<15; i++) {
			oled.pixel(random(oled.getLCDWidth()), random(oled.getLCDHeight()));
			oled.display();
		}
	}

	void BarGraph(uint16_t dataValue) {
		// the moving line
		// +0 is where the next datapoint is drawn
		// +1 is the vertical cursor that traces across the screen
		// +2 is the leading blank line that preceeds the cursor
		uint8_t  OLEDbarY = map(dataValue, oledHRlow, oledHRhigh, 1, OLEDdisplayY);
		bool updateNeeded = false;

		// only advance the line if there is a valid hr reading
		if (HRsenData.heartRate) {
			if   (OLEDbarX<OLEDdisplayX) OLEDbarX++;
			else OLEDbarX=0;
			// Clear the leading edge and add the current heart rate line
			oled.line(OLEDbarX + 0, 0, OLEDbarX, OLEDdisplayY, BLACK, NORM); // clear line
			oled.line(OLEDbarX + 0, OLEDbarY, OLEDbarX, OLEDdisplayY); // draw data
			oled.line(OLEDbarX + 2, 0, OLEDbarX + 2, OLEDdisplayY, BLACK, NORM); // clear leading edge
			updateNeeded = true;
		}
		if (HRsenData.status == 0) { // flash the cursor when no finger detected
			if (oled_flash_state == false) {
				oled.line(OLEDbarX + 1, 0, OLEDbarX + 1, OLEDdisplayY, BLACK, NORM);
				oled_flash_state = true;
			} else {
				oled.line(OLEDbarX + 1, 0, OLEDbarX + 1, OLEDdisplayY);
				oled_flash_state=false;
			}
			updateNeeded = true;
		} else {
			oled.line(OLEDbarX + 1, 0, OLEDbarX + 1, OLEDdisplayY); // draw cursor
		}
		//oled.line(OLEDbarX + 3, 0, OLEDbarX + 3, OLEDdisplayY, BLACK, NORM);
		if (updateNeeded) oled.display();
		// move x forward for the next datapoint
	}
#endif