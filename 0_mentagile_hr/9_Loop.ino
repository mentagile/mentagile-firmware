// continious loop which will run over and over with all functional processes
void loop() {
	if(Serial) {   //  do this main loop until the serial connection is terminated
		// check if there are Msgs or requests from the host               // includes various handshaking protocols

		if (checkHostMsg and (nowish > timeForNextHostMessageCheck)) {
			timeForNextHostMessageCheck =  nowish + HOST_CHECK_INTERVAL;
			readHostMsg();
		}

		readHRsensor();

		#if OLED_disp_used
			if (nowish > timeToUpdateOLED ) { //and (HRsenData.heartRate) )
				timeToUpdateOLED = nowish + OLED_UPDATE_INTERVAL
				BarGraph(HRsenData.heartRate);
			}
		#endif

		readHRsensor();

		// analyse current state using TensorFlowLite model
		//TFlowAnalysis();

		// // Data Shield Red led blinking
		// redLedState = !redLedState;                // change data-shield red led status
		// digitalWrite(red__LEDshield, redLedState); // write red LED status


		//void updateDisplay ();
	} else { // end while(Serial)
		while (!Serial) {
			#if OLED_disp_used
				oled.clear(PAGE);
				printTitle("Mentagile.com", 0);
				printTitle("waiting for comms", 0);
			#endif
      	delay(100);  // no serial, wait for a new serial connection, which should reset the board and run setup again
		}
    resetFunc(); // reset with serial connection is not guaranteed depending on the Arduino board used, so this guarantees it and provides consistency
	}
}
