/**************************************************************************************************
 *
 * The code below manages all time protocols which include:
 *     - creating timestamp
 *     - syncing 0fthe Artemis RTC with the host computer
 *
 * ************************************************************************************************/


// get updated time stamp to sync data
// milliseconds since last midnight
void updateTimeStamp () {
	// update the local time stamp in 100th of seconds since the last midnight as on the host computer
	// 24h*60min*60sec*100th = 8'640'000
	//RTCnow.getTime();
	timeStamp = ((((RTCnow.hour * 60 + RTCnow.minute) * 60 + RTCnow.seconds) * 100) + RTCnow.hundredths);
}

// Correct the local timestamp based on the received data
void timeStamp2RCT (int32_t timeStampRcv, uint8_t setTimeFlag = 0 ) {
// maximum expected value from host computer 863999 as time and 9999 as time difference. int24 is just too small for the possible expected values
// if setTimeFlag = 0 means correction based on the difference of (timeStampHost - timeStampBoard)
// if setTimeFlag = 1 means copy the host computers time stamp
	if (setTimeFlag==0) {                                // timeStampRcv= (timeStampHost - timeStampBoard)
		#if helpDebugging
			Serial.print  ("Timestamp was    : ");
			Serial.println(timeStamp);
		#endif
		timeStampRcv = timeStamp + timeStampRcv/2      ;             // correct the difference with a part of the difference
		#if helpDebugging
			Serial.print  ("Timestamp is now : ");
			Serial.print  (timeStampRcv);
			Serial.println("  ;   The correction of the timestamp was devided by 2.");  // devider used in the above formula "timeStampRcv = timeStamp + timeStampRcv/3"
		#endif
	}
    // convert a timestamp into HH:MM:SS:hs
    uint8_t hs_value =  timeStampRcv          % 100  ;             // the modulo or reminder operator gives the hundreds of seconds
    uint8_t SS_value = (timeStampRcv/    100) %  60  ;             // the modulo 60 gives the number of seconds
    uint8_t MM_value = (timeStampRcv/   6000) %  60  ;             // the modulo 60 gives the number of minutes
    uint8_t HH_value = (timeStampRcv/ 360000) %  60  ;             // the modulo 60 gives the number of hours
    #if helpDebugging
		Serial.print   ("MSG, Current time is HH_" );
		Serial.print   (HH_value);
		Serial.print   (": MM_" );
		Serial.print   (MM_value);
		Serial.print   (": SS_" );
		Serial.print   (SS_value);
		Serial.print   ("  hs_" );
		Serial.println (hs_value);
		RTCnow.setTime(HH_value,MM_value,SS_value,hs_value,1,1,20);  // HH,MM,SS,hh,DD,MM,YY date used is 1 jan 2020
		Serial.print   ("MSG, Timestamp after set is :" );
		Serial.println (timeStampRcv);
    #endif
}

/**************************************************************************************************
 *
 **syncing protocol
 *   Request time from computer to which the RedBoard is connected to by sending a "TM, XXXXXXX\n" string.
 *   Wait for the time stamp send by the connected computer in the time stamp as hundreds of seconds
 *   since the last midnight on the connected PC in te format T!, XXXXXXX
 *   PROCESS
 *   1) if the absolute time difference is smaller than the RCTtimeDiffSet value typicall set at 100 (1 sec)
 *      no correction is required so synchronisation process can be ignored
 *      RTCtimeSet flag needs to e set to high
 *   2) if difference is larger or equal to 9999 ms take the host computers time and set it to local RTC
 *   3) follow the protocol below by correcting the local RTC with half the difference
 *
 *       host PC                   |  RedBoard Artemis
 *   ----------------------------------------------------------------------------
 *                                 |  RTCtimeSet = 0                 // the RCT is not synchronised BOOLEAN
 *                                 |  RCTtimeDiffSet  =  50          // acceptable difference in time of 0.5 secs)
 *                                 |  set timeDiff = timeDiffSet     // time difference set is the max acceptable difference
 *                                 |
 *                                 |  As long as RTCtimeSet == 0
 *                                 <-   TM, XXXXXXX\n                // where XXXXXXX = Local time stamp
 *       TD, YYYY,  XXXXXXX\n     ->    // PC host returns this string in which
 *                                 |    // YYYY the difference between host-RTC - Atemis-RTC (no leading zeros required)
 *                                 |    // if difference is larger than  9998 send YYYY = 9999
 *                                 |    // if difference is less   than -9998 send YYYY = 9999 or -9999
 *                                 |    // Value 0 ass YYYY is ignored by Artemis board as it is considered an error e.f. NA error
 *                                 |    If abs(YYYY) == 9999  take the XXXXXXX host time stamp and use it Artemis board
 *                                 <-   TM, XXXXXXX\n
 *       TD, YYYY,  XXXXXXX\n      ->
 *                                 |    Check difference between local and send time stamp
 *                                 |    If abs(difference) is < timeDiffSet
 *                                 |      abs(difference) = timeDiff
 *                                 |      Correct local time with 1/2 of timestamp differences
 *                                 |      RTCtimeSet =1
 *                                 |    Once synchronised not TM string are send to host computer
 *      File format description
 *      -----------------------
 *      TD, YYYY,  XXXXXXX\n
 *          YYYY,             Difference between the timestamp received and local >= |999|
 *                 XXXXXXX    Time stamp
 *
 *      Timestamp format
 *      ----------------
 *      timeStamp = (((((hour * 60 + minute) * 60) + seconds) * 100) + hundredths);
 *
 **************************************************************************************************/

// sync RTC clock on Red boardRCT with the clock of the external host computer