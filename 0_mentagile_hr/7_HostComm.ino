/********************************************************************
 *
 * The code below manages the handshaking protocols with the host computer.
 * These include:
 *   - the information required for syncing of the Real Time Clock on the Artemis board
 *     with the time on the host computer
 *   - changing some of the sensor settings from the host computer and some overall program parameters
 * It excludes:
 *   - broadcasting of sensor data
 *   - debugging data if flag is said
 *
 * Expected MSG from host computer are:
 *   - TD, (time difference), (host computer timestamp)              // provides information for synchronisation
 *     => Synchronisation protocol
 *   - TM                                                            // provides readable time from the Artemis board
 *     => MSG, Current time is HH_hh: MM_mm: SS_ss: hs_xx
 *   - VR                                                            // provides firmware version
 *     => MSG, Firmware ver.: YYMMDD#
 *
 *
 ********************************************************************/
void readHostMsg () {
  	// sent timeStamp out if RTCs are not synchronised to initiate synchronisation with hostcomputer
	if (!RTCtimeSet) {
		Serial.print  ("TM, ");
		//updateTimeStamp ();
		Serial.println();
	}
	// read the string received from host computer
	String hostStringRcv = "";                                         // string to capture data from USB serial port provides the data required for the code below
  	// Read setting for RCT, filters and buffers
	if (Serial.available() > 0) {                                         // if nothing is waiting on the serial (USB) receive buffer, don't bother
		// read the incoming string
		hostStringRcv   = Serial.readString();                           // read the file from the host computer
		// TD
		// check the TimeDifference (TD) block information
		// check if the expected TD, header is there and RTC is not set yet
		if (hostStringRcv.substring(0,2) == "TD" && !RTCtimeSet ) {        // expected string of format TD, YYYY,  XXXXXXX\n of none fixed lenght (no leading zeros used)
			int8_t commaLoc       ;                                        // position of comma in a string
			String tempString     ;                                        // temporary string for data recovery
			int16_t timeDiff      ;                                        //

			// debugging information
			#if helpDebugging                                              // lines used to help debugging
				Serial.print  ("MSG, TD string received with value:  ");
				Serial.println(hostStringRcv);
			#endif

			// green led on data shield should blink with a state change for every synchronisation request
			greenLedState = !greenLedState         ;                  // make green LED blink to indicate the synchronisation process
			digitalWrite( greenLEDshield,  greenLedState);                 // change green LED status on data shield
			hostStringRcv = hostStringRcv.substring(3,hostStringRcv.length()); // take the string without the first three caracters
			commaLoc = hostStringRcv.indexOf(',');               // check where the first comma is located string lenght < 255 characters
			tempString = hostStringRcv.substring(0,commaLoc);      // take the part of the string that has the first set of numbers
			timeDiff = tempString.toInt();                       // convert string to an integer, if the string are not digits the result is 0 (zero)

			// debugging information
			#if helpDebugging
				Serial.print  ("MSG, time difference from TD string is:  ");
				Serial.println(timeDiff);
			#endif

			// when the synchronisation is good enough for our current purpose confirm else correct
			if ((abs(timeDiff) < RCTtimeDiffSet) && timeDiff!=0 ) {          // if the string is non numeric the value becommes 0 so this value is excluded to avoid problems
			    // excluding 0 means the clock is likely not to be fully synchronised which is acceptable in this case.
				#if helpDebugging
					Serial.println("MSG, The time difference between host-clock and Artemis-RTC between 0 and RCTtimeDiffSet excluding both values");
				#endif
				// confirm that the real time clock is synchronised with host computer
				Serial.println("MSG, Clocks are synchronised!");
				RTCtimeSet = HIGH  ;                                      // the clock is synchronised enough and no atteps will be made to further sync ths clocks
				digitalWrite(greenLEDshield,  HIGH);                         // green led of AdaFruit data shield (if used) is set on. The data shield is a help but not required for the project!
			} else {  // Artemis RTC not synchronised yet
				int32_t timeStampHost ;
				// if the the time difference is too high take the time from the host computer
				if (abs(timeDiff) == 9999) {
					// time difference is too large so timeStamp is coppied from hostcomputer
					// debugging information
					#if helpDebugging
						Serial.println("MSG, The time |difference| between clocks is equal or larger than |9999| ");
					#endif
					tempString         = hostStringRcv.substring(commaLoc+1,hostStringRcv.length()); // take the string without the time difference information
					timeStampHost      = tempString.toInt();                   // convert string to an integer. If the string are not digits the result is 0 (zero) so this needs excluding
																				// the unlikely event the difference is really 0 milliseconds, a higher precission than we are looking for.
					if (timeStampHost) {                                         // check if the timestampHost !=0
						timeStamp2RCT(timeStampHost, 1);                         // second value 0 for time difference or 1 for timestamp
						Serial.println("MSG, Host time has been copied to Artemis-RCT");
					}
				} else {
					if (timeDiff)                                              // check if value is not zero (possitive or negative)
					timeStamp2RCT(timeDiff, 0);                                // send time difference for correction
				}
			}  // end of else statement when Artemis RTC is not synchronised yet
		}    // end if string is "TD"

		// TM send from host will provide a message with a real time conversion of the time stamp
		// in a human readable form ;-)
		if (hostStringRcv.substring(0,2) == "TM") {                        // expected string of format TM with anything
			updateTimeStamp ();                                            // get the updated timestamp
			// convert a timestamp into HH:MM:SS:hs
			uint8_t hs_value =  timeStamp          % 100  ;                // the modulo or reminder operator gives the hundreds of seconds
			uint8_t SS_value = (timeStamp/    100) %  60  ;                // the modulo 60 gives the number of seconds
			uint8_t MM_value = (timeStamp/   6000) %  60  ;                // the modulo 60 gives the number of minutes
			uint8_t HH_value = (timeStamp/ 360000) %  60  ;                // the modulo 60 gives the number of hours
			Serial.print   ("MSG, Current time is HH_" );
			Serial.print   (HH_value);
			Serial.print   (": MM_" );
			Serial.print   (MM_value);
			Serial.print   (": SS_" );
			Serial.print   (SS_value);
			Serial.print   ("  hs_" );
			Serial.println (hs_value);
		}

		// VR
		// responce to the request of the firmware version
		if (hostStringRcv.substring(0,2) == "VR") {                        // expected string of format VR with anything
			Serial.print  ("MSG, Firmware ver.: ");                        // just inform such string was received
			Serial.println(firmwareVer);
		}    // end if string is "VR"
	}      // end if serial data is available
}        // end readHostMsg()
