/*********************************************************************
 *
 *   Code created:
 *      - FOR: "Machine Learning in the Real World" Hacker.io competition in collaboratio with SparkFun
 *        using SparkFun's RedBoard Artemis ATP, Qwiic components, and machine learning
 *        to solve everyday problems.
 *      - BY: K.Bostoen, K.Sobol, B.Jackson (2020)
 *   Released under a Creative Common Attribution-ShareAlike 4.0 International license unless specified in code snippets
 *   CC BY-SA 4.0 more information on the license https://creativecommons.org/licenses/by-sa/4.0/
 *      - BY: K.Bostoen, K.Sobo0l, B.Jackson (2020)
 *      - SA: For the ShareAlike please inform us at: INFO@mentAgile.org
 *
 *   IDE: Arduino 1.8.10
 *
 *    Distributed as-is; no warranty is given in any way, shape or form
 *********************************************************************
 *
 *    We aimed to credit people as much as possible as all is build on
 *
 ***Code for heart rate and blood oxygen level based on SparkFun Example1_config_BPM_Mode1
 *    Author: Elias Santistevan (SparkFun Electronics)
 *    Date: 8/2019
 *    License: No infomation available
 *
 *    HR sensor hardware connections are as follows:
 *    HR sensor            Artemis                                                   MentAgile (our) setup
 *      SDA   -------------- SDA    Qwiic cable connection                           or Flatcable Orange
 *      SCL   -------------- SCL    Qwiic cable connection                           or Flatcable Yellow
 *      RESET -------------- PIN 27(ATP) PIN 6 (Blue  wire in SparkFun Hookup guide) or Flatcable Green
 *      MFIO  -------------- PIN 28(ATP) PIN 7 (White wire in SparkFun Hookup guide) or Flatcable Blue
 *      3V3   -------------- 3V3    Qwiic cable connection                           or Flatcable Red
 *      GND   -------------- GND    Qwiic cable connection                           or Flatcable Brown
 *
 ***Code for optional micro OLED display
 *    Author:  Jim Lindblom (SparkFun Electronics)  Modified by:  Joel Bartlett
 *    Date:    26 oct 2014                          Date:         12 Apr 2017
 *    License: No infomation available              License:      Beerware ;-) to Joel
 *
 *    Micro OLED display connections are as follows using I2C:
 *    Micro OLED                   Artemis Redboard (or ATP)
 *      GND ------------------------ GND                          Qwiic cable connection
 *      VDD ------------------------ 3.3V (VCC)                   Qwiic cable connection
 *      SDA ------------------------ D11  (fixed don't change)    Qwiic cable connection
 *      SCL ------------------------ D13  (fixed don't change)    Qwiic cable connection
 *      RST (declared not connected) D9   (any digital pin)       required by the micro OLED library
 *
 *    If you run into an error code for the HR sensoor check the following table to help diagnose your problem:
 *      1 = Unavailable Command
 *      2 = Unavailable Function
 *      3 = Data Format Error
 *      4 = Input Value Error
 *      5 = Try Again
 *      255 = Error Unknown
 *
 ***Artemis RedBoard RTC code based on Artemis RCT example1_getTime
 *    Author: Nathan Seidle
 *    Created: Septempter 27th, 2019
 *    License: MIT. See SparkFun Arduino Apollo3 Project for more information
 *             based on the Ambiq SDK EVB2 RTC example.
 *
 *
 *  Distributed as-is; no warranty is given in any way shape or form
 *******************************************************************/
