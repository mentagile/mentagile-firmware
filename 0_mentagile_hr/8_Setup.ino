// used to force a reset
void(* resetFunc) (void) = 0; //declare reset function @ address 0
//startDateTime = String(RTCnow.seconds);
// setup code which will run only once at start to initialise all processes
void setup() {
  // set speed for serial communications
	Serial.begin(BaudRate);

	#if OLED_disp_used
		// determine OLED display information
		OLEDdisplayX = oled.getLCDWidth ();
		OLEDdisplayY = oled.getLCDHeight();
		OLEDmiddleX  = OLEDdisplayX/2;
		OLEDmiddleY  = OLEDdisplayY/2;
		maxDataValue = min(OLEDmiddleX, OLEDmiddleY);

		Wire.begin();      // initiate the I2C communication
		oled.begin();      // initialize the OLED
		oled.clear(PAGE);  // clear the buffer.
		oled.clear(ALL);   // clear the display's internal memory
		randomPixel();     // Add some sparkel to the display
		printTitle("Mentagile.com", 0, 1);
		randomPixel();     // Add some sparkel to the display
		oled.clear(PAGE);
	#endif

	// for now, any incoming command will trigger the data flow
	while (!Serial.available()) {  // waits for communications to begin
		#if OLED_disp_used
			printTitle("waiting", 0, 1);
			Serial.println("RUREADY");
			randomPixel();
		#else
			Serial.println("RUREADY");
			delay(1000);
		#endif
	}
	#if OLED_disp_used
		oled.clear(PAGE);
	#endif

	// declare pin functions
	pinMode(red__LEDshield,   OUTPUT);                                 // initialize digital red__LEDShield pin as an output to connect with Adafruit data shield if used
	pinMode(greenLEDshield,   OUTPUT);                                 // initialize digital greenLEDShield pin as an output to connect with Adafruit data shield if used

	// welcome message
	Serial.print  (PRIMARYIDENTIFIER + millis() + ", MSG, BioQuest Hacker.io/SparkFun AI firmware ver.: ");
	Serial.println(firmwareVer);
	Serial.println(PRIMARYIDENTIFIER + millis() + ", MSG, Primary Identifier, Sequential Monotonic Identifier, Secondary Identifier, Value(s)");
	Serial.println(PRIMARYIDENTIFIER + millis() + ", MSG, 	Where Primary Identifier is the device data source: 'SFRB' in this case (Spark Fun Red Board)");
	Serial.println(PRIMARYIDENTIFIER + millis() + ", MSG, 	Sequential Monotonic Identifier is an always increasing integer: milliseconds since epoch currently");
	Serial.println(PRIMARYIDENTIFIER + millis() + ", MSG, 	Secondary Identifier is the specific type of data from the device: 'HR' for heartrate and value(s) is the corresponding value.");
	Serial.println(PRIMARYIDENTIFIER + millis() + ", MSG, 	Secondary Identifiers currently are, but not limited to: MSG (informational system message, ERR (error), ");
	Serial.println(PRIMARYIDENTIFIER + millis() + ", MSG, 	HR (heartrate), HRc (heartrate confidence), SPO2 (oxygen saturation), IR (Infrared LED measurement), RED (Red LED measurement), ");
	Serial.println(PRIMARYIDENTIFIER + millis() + ", MSG, 	rVal (SP02 rValue), xSt (extended status: 0=success, 1=not ready, -1=object detected, -2=excessive sensor device motion, ");
	Serial.println(PRIMARYIDENTIFIER + millis() + ", MSG, 	-3=no object detected, -4=pressing too hard, -5=object other than finger detected, -6=excessive finger motion)");
	Serial.println(PRIMARYIDENTIFIER + millis() + ", MSG, 	St (status: 0=no object detected, 1=object detected, 2=object other than finger detected, 3=finger detected)");
	Serial.print(PRIMARYIDENTIFIER + millis() + ", MSG, resPin: ");
	Serial.print(resPin);
	Serial.print(PRIMARYIDENTIFIER + millis() + ", MSG, mfioPin: ");
	Serial.println(mfioPin);

	// set RTC on redboard to compilation time at compilation
	RTCnow.setToCompilerTime();                                       // set RTC using the system __DATE__ and __TIME__ macros from compiler
	if (RTCtimeSet) {
		Serial.println(PRIMARYIDENTIFIER + millis() + ", MSG, RTC synchronisation with PC will be ignored."                    );
		Serial.println(PRIMARYIDENTIFIER + millis() + ", MSG, If firmware was not compiled RTC might hold incorrect time."     );
	}
	Serial.print  (PRIMARYIDENTIFIER + millis() + ", MSG, Time now is  ");
	updateTimeStamp ();
	Serial.print  (RTCnow.hour);
	Serial.print  (":");
	Serial.print  (RTCnow.minute);
	Serial.print  (":");
	Serial.print  (RTCnow.seconds);
	Serial.print  ("TimeStamp (100th of sec) : ");
	Serial.println(timeStamp);

	// setup communication with the heart rate & blood oxygen sensor
	Wire.begin();
	int result = bioHub.begin();
	lastNowish = millis();  // marks the start of the sensor data buffer time (see HR_data.ino)
	if (result == 0)                             // there were no errors when establishing communication with sensor
		Serial.println(PRIMARYIDENTIFIER + millis() + ", MSG, Communication with HR sensor established.");
	else
		Serial.println(PRIMARYIDENTIFIER + millis() + ", ERR, Could not communicate with the sensor!");

	// configurating heart rate & blood oxygen sensor MODE
	// MODE_ONE for basic & MODE_TWO for advanced sensor settings.
	Serial.print(PRIMARYIDENTIFIER + millis() + ", MSG, Configuring the Oximeter HR Sensor to MODE_");
	Serial.println (HRsensorMode);
	int error = bioHub.configSensorBpm(HRsensorMode);
	if (error == 0) { // there were no errors when configurating the sensor
		Serial.println(PRIMARYIDENTIFIER + millis() + ", MSG, Heart rate & blood oxygen sensor configured.");
	} else {
		// error message in case there is problem setting up the heart rate & blood oxygen sensor
		Serial.println(PRIMARYIDENTIFIER + millis() + ", MSG, Error configuring heart rate & blood oxygen sensor.");
		Serial.print  (PRIMARYIDENTIFIER + millis() + ", ERR, ");
		Serial.println(PRIMARYIDENTIFIER + millis() + error);
		Serial.println(PRIMARYIDENTIFIER + millis() + ", MSG,   ----------------");
		Serial.println(PRIMARYIDENTIFIER + millis() + ", MSG,    List of errors");
		Serial.println(PRIMARYIDENTIFIER + millis() + ", MSG,   ----------------");
		Serial.println(PRIMARYIDENTIFIER + millis() + ", MSG,   1 = Unavailable Command");
		Serial.println(PRIMARYIDENTIFIER + millis() + ", MSG,   2 = Unavailable Function");
		Serial.println(PRIMARYIDENTIFIER + millis() + ", MSG,   3 = Data Format Error");
		Serial.println(PRIMARYIDENTIFIER + millis() + ", MSG,   4 = Input Value Error");
		Serial.println(PRIMARYIDENTIFIER + millis() + ", MSG,   5 = Try Again");
		Serial.println(PRIMARYIDENTIFIER + millis() + ", MSG, 255 = Error Unknown");
	}
	// adjusting individual parameters of the HR sensor
	// adjusting the Automatic Gain Control (AGC) algorithm value of the HR sensor
	error = bioHub.setAlgoRange(HRalgoRange);
	if (error > 0) {
		Serial.println(PRIMARYIDENTIFIER + millis() + ", ERR, Could not set the HR sensor algorithm's Range.");
	} else { // read and print the Automatic Gain Control (AGC) algorithm value of the HR sensor
		Serial.print(PRIMARYIDENTIFIER + millis() + ", MSG, The HR sensor algorithm's AGC value set to: ");
		Serial.println (PRIMARYIDENTIFIER + millis() + bioHub.readAlgoRange());
	}

	// adjusting the StepSize algorithm value of the HR sensor
	error = bioHub.setAlgoStepSize(HRalgoStepSize);
	if (error > 0) {
		Serial.println (PRIMARYIDENTIFIER + millis() + ", ERR, Could not set the HR sensor algorithm's step size.");
	} else {
		Serial.print   (PRIMARYIDENTIFIER + millis() + ", MSG, The HR sensor algorithm's step size is set to: ");
		Serial.println (bioHub.readAlgoStepSize());
	}

	//  adjusting the sensitivity algorithm value of the HR sensor
	error = bioHub.setAlgoSensitivity(HRalgoSens);
	if (error > 0) {
		Serial.println (PRIMARYIDENTIFIER + millis() + ", ERR, Could not set the HR sensor algorithm's sensitivity.");
	} else {
		Serial.print   (PRIMARYIDENTIFIER + millis() + ", MSG, The HR sensor algorithm's sensitivity is set to: ");
		Serial.println (bioHub.readAlgoSensitivity());
	}

	// adjusting the sample size algorithm value of the HR sensor
	error = bioHub.setAlgoSamples(HRalgoSamp);
	if (error > 0) {
		Serial.println (PRIMARYIDENTIFIER + millis() + ", ERR, Could not set the HR sensor algorithm's sample size.");
	} else {
		Serial.print   (PRIMARYIDENTIFIER + millis() + ", MSG, The HR sensor algorithm's sample size is set to: ");
		Serial.println (HRalgoSamp);
	}

}
