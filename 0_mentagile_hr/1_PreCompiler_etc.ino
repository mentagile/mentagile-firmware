// Common User Settings
#define OLED_disp_used 0    // to add or remove micro OLED library references. Display is optional
#define Artemis_ATP 1         // 1 for Artemis RedBoard ATP 0 for "Regular Artemis RedBoard"

// define values for pre-compiling which allow to make the code more readable without reserving space for variables
// conditionalities for preprocessing.
#define firmwareVer 2002111   // YYMMDD# the current firmware version date with a number in case more versions in one day
#define helpDebugging 0           // 0 or 1 (set 1 for extra serial messages to help debugging
#define blinkTimeErr 100         // time of on and off state of led when blinking for error
#define BaudRate 230400          // baudrate for serial communications
#define RCTtimeDiffSet 50       // acceptable differenct between RCT clock and the clock of the host computer in 100th of seconds
#define TimeCorrDevider 2      // Devider for the correction (timeStampHost - timeStampBoard) / TimeCorrDevider
#define HRsensorMode MODE_ONE // MODE_TWO // "MODE_ONE" basic, "MODE_TWO" ADVANCED

// these values are fixed for now but will be controled by host computer in future
#define checkHostMsg HIGH  //HIGH // check messages from host computer
#define HRalgoRange 80        // ADC Range   (0-100%)
#define HRalgoStepSize 20   // Step Size   (0-100%)
#define HRalgoSens 20          // Sensitivity (0-100%)
#define HRalgoSamp 1          // Number of samples to average (0-255)
#define HOST_CHECK_INTERVAL 2000   // check for host communication every 2 seconds

const String PRIMARYIDENTIFIER = "SFRB, ";

/**************************************************************************************************
 *
 *  Pulse Width vs. Sample Collection for the HR sensor
 *  All possible values and combinations are listed below
 *  Trade off between higher resolution (i.e. longer pulse width) and the number of samples per second.
 *  Samples/Second  |  Pulse Width (uS)
 *  ----------------|  69  118 215 411
 *    50            |  I   I   I   I
 *   100            |  I   I   I   I
 *   200            |  I   I   I   I      ( I  denotes a    possible combination )
 *   400            |  I   I   I   I      ( 0  denotes an impossible combination )
 *   800            |  I   I   I   0
 *  1000            |  I   I   0   0
 *  1600            |  I   0   0   0
 *  -----------------------------------
 *  Resolution (bits)  15  16  17  18
 *
 **************************************************************************************************/

// The pin numbering on the Artemis RedBoard and the Artemis RedBoard ATP are not identical
// The following lines allow to use either board if the setting earlier in the code are correct
#if (Artemis_ATP)
	// pins connecting to the HR sensor
	#define resPin 27  // pin used for resetting heart rate and blood oxygen sensor
	#define mfioPin 28 // multiFunctional In&Out pin or MFIO for the heart rate and blood oxygen sensor
	// pins conecting to data shield if used
	#define red__LEDshield 22 // red   led on Adafruit data shield when shield is used
	#define greenLEDshield 4  // green led on Adafruit data shield when shield is used
#else
	// pins connecting to the HR sensor
	#define resPin 6         // pin used for resetting heart rate and blood oxygen sensor
	#define mfioPin 7        // multiFunctional In&Out pin or MFIO for the heart rate and blood oxygen sensor
	// pins conecting to data shield if used
	#define red__LEDshield 4 // red   led on Adafruit data shield when shield is used
	#define greenLEDshield 3 // green led on Adafruit data shield when shield is used
#endif

// adding libraries
#include <RTC.h>                             // include the RTC library of the Aruino_Apollo3 core
#include "SparkFun_Bio_Sensor_Hub_Library.h" // library for SparkFun HR sensor the " " instead of the < > means
// #include <SparkFun_Bio_Sensor_Hub_Library>  // it is looking for a local library e.g. output is now HR*10
#include <string.h>                          // required for tensorFlow lib
unsigned long timeForNextHostMessageCheck = 1000;  // ms since start of the program

#if OLED_disp_used                     // values above allow to include or exclude library as required
	#include <Wire.h>                            // include Wire as we are using I2C through the Qwiic connection
	#include <SFE_MicroOLED.h>                   // library for micro organic light-emitting diode (OLED) display
	// pins "connecting" to the micro OLED display
	#define OledResetPin 9 // although the reset is hardwired on the display the library expects its declaration \
    // this is a legacy issue and any not used digital pin can be used here
	#define OledAddress 1  // indicates the default I2C Address is selected; 1 if the jumper is open (Default), set 0 if it's closed
	#define OLED_UPDATE_INTERVAL 800;  // update the OLED display 1 times per 0.8 seconds
	#define OLED_START_HR_LOW 600 // the bottom of the HR graph (hr*10)
	#define OLED_START_HR_HIGH 1200 // the top of the HR graph (hr*10)
	MicroOLED oled(OledResetPin, OledAddress);       // I2C declaration
	bool oled_flash_state = false; // used to flash the cursor if HR is not reading
	unsigned long timeToUpdateOLED = 4000; // ms since the start of the program to  update the OLED
	//   MicroOLED variables
	uint8_t OLEDdisplayX;  // with for display less than 254 pixels wide
	uint8_t OLEDdisplayY;  // with for display less than 254 pixels high
	uint16_t OLEDbarX = 0; // X location of data bar
	uint8_t OLEDmiddleX;   // X of middle of the screen
	uint8_t OLEDmiddleY;   // Y of middle of the screen
	uint8_t maxDataValue;
	uint16_t oledHRlow = OLED_START_HR_LOW;
	uint16_t oledHRhigh = OLED_START_HR_HIGH;
#endif

// creating Objects
APM3_RTC RTCnow;                                 // create instance of RTC class for Artemis RedBoard internal clock
SparkFun_Bio_Sensor_Hub bioHub(resPin, mfioPin); // this command takes the address of the sensor, and declares reset and MFIO pin

/**************************************************************************************************
 *
 *  Create bioData struct
 *  ---------------------
 *  For more info see https://www.norwegiancreations.com/2017/10/getting-started-with-programming-part-8-typedef-and-structs/
 *
 **************************************************************************************************/
bioData HRsenData, lastHRsenData;
char tfLiteSensorBuffer [100][MAXFAST_ARRAY_SIZE + MAX30101_LED_ARRAY];
uint lastNowish = millis(); 
uint nowish = millis();

/**************************************************************************************************
 *
 *  bioaData is a variable type (like int, byte, long) unique to the SparkFun library for the
 *  Pulse Oximeter and Heart Rate Monitor. Unlike those other types it holds specific information on your heartrate and
 *  blood oxygen levels. "bioData" is actually a specific kind of type, known as a "struct".
 *  You can choose any variable name. SparFun uses "body" in their examples we use "HRsenData" as we will use other sensors in future.
 ***MODE_ONE
 *    HRsenData.heartrate  - Heartrate
 *    HRsenData.confidence - Confidence in the heartrate value
 *    HRsenData.oxygen     - Blood oxygen level
 *    HRsenData.status     - Has a finger been sensed?
 *    HRsenData.irLed (diff) - The difference between the current reading and the last reading
 *    HRsenDAta.redLed (diff) - The difference between the current reading and the last reading
 ***MODE_TWO (in addition to MODE_ONE)
 *    HRsenData.extStatus
 *    HRsenData.rValue
 *
 **************************************************************************************************/
boolean RTCtimeSet = LOW;    // high means the time has been set through hanshaking with PC unless set here to ignore synchronisation process
uint32_t timeStamp = 0;      // 100th of seconds since midnight unsigned 32 bit integer
boolean greenLedState = LOW; // the state of the green LED on the AdaFruit dta shield if the shield is use. Shield is NOT
boolean redLedState = LOW;   // the state of the green LED on the AdaFruit dta shield if the shield is use. Shield is NOT

// error message when program runs into problems
void sendErrorMSG(String errorMSG, uint8_t final) {// collect the information of character array of an unknow size (the error message) using a pointer to a memory address I think ;-)
	Serial.print(PRIMARYIDENTIFIER + millis() + ", ERR, ");    // print error to the serial monitor via the USB port
	Serial.println(errorMSG); // followed by the error message
	while (final) {  // loops forever halting the program and printing the error every 5 sec
		//  five seconds  (5000 ms below) of LED flickering before each error message send.
		for (uint8_t i = 0; i < 5000 / (blinkTimeErr + blinkTimeErr); i++) { // depending on the blinkTimeErr set above the LED will go on and of until 5000ms seconds passed.
			// loop blinking for 5 seconds
			digitalWrite(LED_BUILTIN, redLedState);    // and setting the red LED high to indicate an error
			digitalWrite(red__LEDshield, redLedState); // and setting the red LED high to indicate an error
			delay(blinkTimeErr);                       // time of the state with 50% duty cycle
			redLedState = !redLedState;                // change the current state
		}
	}
}

//// -------------------------
//// - begin tflite includes
//// -------------------------
//

//
//// -----------------------
//// - end tflite includes
//// -----------------------
