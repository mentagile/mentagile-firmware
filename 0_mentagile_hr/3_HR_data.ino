 // used to ensure that the sampleTime is monotonic since it back estimates the exact sample time

void readHRsensor() { // information from the readBpm function will be saved to our "body" variable
	// updateTimeStamp();
	int fifoSize = bioHub.numSamplesOutFifo();
	// int irLedChange, redLedChange;
	if (fifoSize > 5) {
		nowish = millis();
		uint sample_spread = (nowish-lastNowish)/fifoSize;
		for  (uint sampleTime = lastNowish + sample_spread; sampleTime < nowish; sampleTime += sample_spread) {
			lastHRsenData = HRsenData;
			HRsenData = bioHub.readSensorBpm();
			// one Serial.println so the send is atomic
			if (HRsenData.heartRate != lastHRsenData.heartRate)
				Serial.println(PRIMARYIDENTIFIER  + sampleTime + ", HR, " + HRsenData.heartRate); //Heart Rate
			if (HRsenData.confidence != lastHRsenData.confidence)
				Serial.println(PRIMARYIDENTIFIER + sampleTime + ", HRc, "+HRsenData.confidence); // Heart Rate Confidence
			if (HRsenData.oxygen  != lastHRsenData.oxygen)
				Serial.println(PRIMARYIDENTIFIER + sampleTime + ", SP02,  "+HRsenData.oxygen); // SPO2 levels
			if (HRsenData.status != lastHRsenData.status)
				Serial.println(PRIMARYIDENTIFIER + sampleTime + ", St, "+HRsenData.status); // status
			if (HRsenData.irLed != lastHRsenData.irLed and HRsenData.irLed > 8192)  // certainly not a reflection
				Serial.println(PRIMARYIDENTIFIER + sampleTime + ", IR, "+HRsenData.irLed); // infrared led levels
			if (HRsenData.redLed != lastHRsenData.redLed and HRsenData.redLed > 8192)  // certainly not a reflection
				Serial.println(PRIMARYIDENTIFIER + sampleTime + ", RED, "+HRsenData.redLed); // red led level

			#if HRsensorMode==MODE_TWO
				if (HRsenData.rValue != lastHRsenData.rValue)
					Serial.println(PRIMARYIDENTIFIER + sampleTime + ", rVal, "+HRsenData.rValue); // SPO2 rValue
				if (HRsenData.extStatus != lastHRsenData.extStatus)
					Serial.println(PRIMARYIDENTIFIER + sampleTime + ", xSt, "+HRsenData.extStatus); // extended status
			#endif
		}
		//Serial.println("fifo: " + String(bioHub.numSamplesOutFifo()));
		lastNowish = nowish;
	}

	/*
	*  Status Number |  Description
	*  ------------------------------------
	*  0             |  No Object Detected
	*  1             |  Object Detected
	*  2             |  Object Other Than Finger Detected
	*  3             |  Finger Detected
	*/

	/*
	*  extended Status Number |  Description
	*  -------------------------------------
	*   0               |  Success
	*   1               |  Not Ready
	*  -1               |  Object Detected
	*  -2               |  Excessive Sensor Device Motion
	*  -3               |  No object detected
	*  -4               |  Pressing too hard
	*  -5               |  Object other than finger detected
	*  -6               |  Excessive finger motion
	*/
}
